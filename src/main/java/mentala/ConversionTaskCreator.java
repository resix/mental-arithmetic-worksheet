package mentala;

import java.util.Random;

public class ConversionTaskCreator {
    public ConversionTask create() {
        int value = new Random().ints(1, 100).findAny().getAsInt();
        boolean switchTask = new Random().nextBoolean();
        Unit originUnit = Unit.getRandomUnit();
        Unit resultUnit = originUnit.getNeighbourUnit();
        ConversionTask conversionTask = new ConversionTask(new ValueWithUnit(value, originUnit), resultUnit);
        ValueWithUnit result = conversionTask.solve();
        if (switchTask) {
            conversionTask = new ConversionTask(new ValueWithUnit(result.getValue(), result.getUnit()), originUnit);
        }
        return conversionTask;
    }
}
