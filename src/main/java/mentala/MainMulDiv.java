package mentala;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class MainMulDiv {

    public static void main(String[] args) throws IOException {
        String taskFileName = "Kopfrechnen.txt";
        String solutionFileName = "KopfrechnenLoesung.txt";
        BufferedWriter taskWriter = new BufferedWriter(new FileWriter(taskFileName));
        BufferedWriter solutionWriter = new BufferedWriter(new FileWriter(solutionFileName));

        TaskCreator taskCreator = new TaskCreator();
        for (int i=0; i<72; i++) {
            Task task = taskCreator.create();
            taskWriter.write(task.toString() + "\n");
            int solution = Double.valueOf(task.solve()).intValue();
            solutionWriter.write(task.toString() + " " + solution + "\n");
        }
        taskWriter.close();
        solutionWriter.close();
        System.out.println("Created tasks in \"" + taskFileName + "\" and solutions in \"" + solutionFileName + "\"!");
    }
}