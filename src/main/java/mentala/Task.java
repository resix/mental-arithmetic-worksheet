package mentala;

public class Task {
    private Operator operator;
    private Integer firstOperand;
    private Integer secondOperand;

    public Task(Operator operator, Integer firstOperand, Integer secondOperand) {
        this.operator = operator;
        this.firstOperand = firstOperand;
        this.secondOperand = secondOperand;
    }

    public double solve() {
        switch (operator) {
            case ADD:
                return firstOperand + secondOperand;
            case SUBTRACT:
                return firstOperand - secondOperand;
            case DIVIDE:
                return firstOperand / secondOperand;
            case MULTIPLY:
                return firstOperand * secondOperand;
            default:
                return -1.0;
        }
    }

    public Integer getFirstOperand() {
        return firstOperand;
    }

    public Integer getSecondOperand() {
        return secondOperand;
    }

    public Operator getOperator() {
        return operator;
    }

    @Override
    public String toString() {
        return firstOperand + " " + operator.symbol + " " + secondOperand + " =";
    }

    public enum Operator {
        ADD("+"),
        SUBTRACT("-"),
        DIVIDE(":"),
        MULTIPLY("*");

        private String symbol;

        Operator(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }
}
