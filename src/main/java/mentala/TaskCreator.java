package mentala;

import java.util.Random;

import static mentala.Task.Operator.DIVIDE;
import static mentala.Task.Operator.MULTIPLY;

public class TaskCreator {

    private Random random = new Random();

    public Task create() {
        int operand1 = new Random().ints(1, 11).findAny().getAsInt();
        int operand2 = new Random().ints(1, 11).findAny().getAsInt();

        Task.Operator operator = random.nextBoolean() ? MULTIPLY : DIVIDE;

        if (operator == MULTIPLY) {
            return new Task(MULTIPLY,
                    operand1 * getMultiplier(4),
                    operand2 * getMultiplier(1));
        } else { // operator == DIVIDE
            Task multiplyTask = new Task(MULTIPLY, operand1, operand2);
            double solution = multiplyTask.solve();

            int multiplierDividend = getMultiplier(4);
            int multiplierDivisor = getMultiplier(3);

            // first operator has to be greater than second for division:
            if (multiplierDividend < multiplierDivisor) {
                int tmp = multiplierDividend;
                multiplierDividend = multiplierDivisor;
                multiplierDivisor = tmp;
            }
            return new Task(DIVIDE,
                    Double.valueOf(solution).intValue() * multiplierDividend,
                    operand2 * multiplierDivisor);
        }
    }

    private int getMultiplier(int max) {
        return Double.valueOf(Math.pow(10, new Random().ints(0, max+1).findAny().getAsInt())).intValue();
    }
}
