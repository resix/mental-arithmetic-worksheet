package mentala;

public class ConversionTask {

    private ValueWithUnit firstOperand;
    private Operator operator;
    private ValueWithUnit secondOperand;
    private Unit resultUnit;

    public ConversionTask(ValueWithUnit firstOperand, Unit resultUnit) {
        this.firstOperand = firstOperand;
        this.resultUnit = resultUnit;
    }

    public ConversionTask(ValueWithUnit firstOperand, Operator operator, ValueWithUnit secondOperand, Unit resultUnit) {
        this.firstOperand = firstOperand;
        this.operator = operator;
        this.secondOperand = secondOperand;
        this.resultUnit = resultUnit;
    }

    public ValueWithUnit solve() {
        if (secondOperand == null) {
            double factor;
            if (resultUnit.getFactor() < firstOperand.getUnit().getFactor()) {
                factor =  firstOperand.getUnit().getFactor() / resultUnit.getFactor();
                return new ValueWithUnit(firstOperand.getValue() / factor, resultUnit);
            } else {
                factor = resultUnit.getFactor() / firstOperand.getUnit().getFactor() ;
                return new ValueWithUnit(firstOperand.getValue() * factor, resultUnit);
            }
//            This is a nice formula that has rounding issues :-(
//            return new ValueWithUnit(firstOperand.getValue() * resultUnit.getFactor() / firstOperand.getUnit().getFactor(), resultUnit);
        } else {
            return new ValueWithUnit(0.0, new Unit(1, "unknown"));
        }
    }

    @Override
    public String toString() {
        if (secondOperand == null) {
            return firstOperand + " =";
        } else {
            return firstOperand + " " + operator.symbol + " " + secondOperand + " =";
        }
    }

    public enum Operator {
        ADD("+"),
        SUBTRACT("-"),
        DIVIDE(":"),
        MULTIPLY("*");

        private String symbol;

        Operator(String symbol) {
            this.symbol = symbol;
        }

        public String getSymbol() {
            return symbol;
        }
    }
}
