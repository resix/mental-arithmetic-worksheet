package mentala;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import static java.util.Arrays.asList;

public class Unit {
    private static final Random RANDOM = new Random();
    private static final List<Length> LENGTH_VALUES = Collections.unmodifiableList(asList(Length.values()));
    private static final List<Weight> WEIGHT_VALUES = Collections.unmodifiableList(asList(Weight.values()));

    public double getFactor() {
        return factor;
    }

    public String getSymbol() {
        return symbol;
    }

    private final double factor;
    private final String symbol;

    public Unit(double factor, String symbol) {
        this.factor = factor;
        this.symbol = symbol;
    }

    public static Unit getRandomUnit() {
        if (new Random().nextBoolean()) {
            Length length = LENGTH_VALUES.get(RANDOM.nextInt(LENGTH_VALUES.size()));
            return new Unit(length.getFactor(), length.getSymbol());
        } else {
            Weight weight = WEIGHT_VALUES.get(RANDOM.nextInt(WEIGHT_VALUES.size()));
            return new Unit(weight.getFactor(), weight.getSymbol());
        }
    }

    public Unit getNeighbourUnit() {
        if (LENGTH_VALUES.stream().filter(x -> x.getSymbol().equals(symbol)).findAny().isPresent()) {
            // this is a length unit
            Length length = LENGTH_VALUES.get(rotate(Length.getIndex(symbol), LENGTH_VALUES.size()));
            return new Unit(length.getFactor(), length.getSymbol());
        } else {
            // this is a weight unit
            Weight weigth = WEIGHT_VALUES.get(rotate(Weight.getIndex(symbol), WEIGHT_VALUES.size()));
            return new Unit(weigth.getFactor(), weigth.getSymbol());
        }
    }

    public enum Length {
        MM(1000.0, "mm"),
        CM(100.0, "cm"),
        DM(10.0, "dm"),
        M(1.0, "m"),
        KM(0.001, "km");

        private final double factor;
        private final String symbol;

        Length(double factor, String symbol) {
            this.factor = factor;
            this.symbol = symbol;
        }

        public double getFactor() {
            return factor;
        }

        public String getSymbol() {
            return symbol;
        }

        public static int getIndex(String symbol) {
            for (int i = 0; i < LENGTH_VALUES.size(); i++) {
                if (symbol.equals(LENGTH_VALUES.get(i).getSymbol())) {
                    return i;
                }
            }
            return -1;
        }
    }

    public enum Weight {
        MG(1000.0, "mg"),
        G(1.0, "g"),
        KG(0.001, "kg"),
        T(0.000001, "t");

        private final double factor;
        private final String symbol;

        Weight(double factor, String symbol) {
            this.factor = factor;
            this.symbol = symbol;
        }

        public double getFactor() {
            return factor;
        }

        public String getSymbol() {
            return symbol;
        }

        public static int getIndex(String symbol) {
            for (int i = 0; i < WEIGHT_VALUES.size(); i++) {
                if (symbol.equals(WEIGHT_VALUES.get(i).getSymbol())) {
                    return i;
                }
            }
            return -1;
        }
    }

    private static int rotate(int index, int size) {
        if (index == 0) {
            return 1;
        } else if (index == size - 1) {
            return size - 2;
        } else {
            boolean up = RANDOM.nextBoolean();
            return up ? index + 1 : index - 1;
        }
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Unit
                && this.factor == ((Unit) obj).factor
                && this.symbol.equals(((Unit) obj).symbol);
    }
}
