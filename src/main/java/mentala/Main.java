package mentala;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

public class Main {

    public static void main(String[] args) throws IOException, DocumentException {
        String taskFileName = "MatheTest";
        String solutionFileName = "MatheTestLoesung";
        BufferedWriter taskWriter = new BufferedWriter(new FileWriter(taskFileName + ".txt"));
        BufferedWriter solutionWriter = new BufferedWriter(new FileWriter(solutionFileName + ".txt"));

        Document testDocument = new Document();
        Document solutionDocument = new Document();
        PdfWriter.getInstance(testDocument, new FileOutputStream(taskFileName + ".pdf"));
        PdfWriter.getInstance(solutionDocument, new FileOutputStream(solutionFileName + ".pdf"));
        testDocument.open();
        solutionDocument.open();

        PdfPTable testTable = new PdfPTable(3);
        testTable.setWidthPercentage(100);
        PdfPTable solutionTable = new PdfPTable(3);

        testDocument.add(new Paragraph(new Chunk("Name: ___________                               Klasse:___              Datum:__________\n\n")));
        testDocument.add(new Paragraph(new Chunk("                                              Kopfrechen - Test\n\n")));

        TaskCreator taskCreator = new TaskCreator();
        for (int i=0; i<28; i++) {
            Task task = taskCreator.create();
            taskWriter.write(task.toString() + "\n");
            int solution = Double.valueOf(task.solve()).intValue();
            String solutionString = task.toString() + " " + solution;
            solutionWriter.write(solutionString + "\n");
            PdfPCell taskCell = new PdfPCell(new Phrase(task.toString() + "\n "));
            taskCell.setBorder(Rectangle.LEFT);
            testTable.addCell(taskCell);
            solutionTable.addCell(solutionString);
        }

        ConversionTaskCreator conversionTaskCreator = new ConversionTaskCreator();
        for (int i=0; i<24; i++) {
            ConversionTask conversionTask = conversionTaskCreator.create();
            String conversionTaskString = conversionTask + "_____________" + conversionTask.solve().getUnit().getSymbol();
            taskWriter.write(conversionTaskString + "\n");
            String solutionString = conversionTask + "\t" + conversionTask.solve();
            solutionWriter.write(solutionString + "\n");
            PdfPCell taskCell = new PdfPCell(new Phrase(conversionTaskString + "\n "));
            taskCell.setBorder(Rectangle.LEFT);
            testTable.addCell(taskCell);
            solutionTable.addCell(solutionString);
        }
        for (int i=0; i<5; i++) {
            java.util.List<Character> list = new ArrayList<>();
            list.add('\u00B2');
            list.add('\u00B3');
//            list.add('\u2074');

            int index = new Random().nextInt(list.size());
            int value = new Random().ints(2, 7).findAny().getAsInt();
            Character exponent = list.get(index);
            String taskString = value + "" + exponent;
            PdfPCell taskCell = new PdfPCell(new Phrase(taskString + " =\n"));
            taskCell.setBorder(Rectangle.LEFT);
            testTable.addCell(taskCell);
            int exponentValue = exponent == '\u00B2' ? 2 : 3;
            solutionTable.addCell(taskString + "=" + new DecimalFormat("0.###").format(Math.pow(value, exponentValue)));
        }
        taskWriter.close();
        solutionWriter.close();

        testDocument.addTitle("Kopfrechen - Test");
        testDocument.add(testTable);
        int totalTasks = testTable.getRows().size() * testTable.getNumberOfColumns();
        testDocument.add(new Paragraph(new Chunk("\n" + "Punkte: ___/" + totalTasks+ "\n\n")));
        testDocument.add(new Paragraph(new Chunk("Note: ______" + "\n\n")));
        testDocument.add(new Paragraph(new Chunk("Unterschrift:" + "\n\n" + "_______________________")));
        solutionDocument.add(solutionTable);
        testDocument.close();
        solutionDocument.close();

        System.out.println("Created tasks in \"" + taskFileName + "\" and solutions in \"" + solutionFileName + "\"!");
    }
}