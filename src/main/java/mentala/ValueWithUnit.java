package mentala;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class ValueWithUnit {
    double value;
    Unit unit;
    private static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("0.###");

    static {
        DECIMAL_FORMAT.setDecimalFormatSymbols(new DecimalFormatSymbols(Locale.GERMANY));
    }

    public ValueWithUnit(double value, Unit unit) {
        this.value = value;
        this.unit = unit;
    }

    public double getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return DECIMAL_FORMAT.format(value) + " " + unit.getSymbol();
    }

}
