package mentala;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;


public class MainConversion {

    public static void main(String[] args) throws IOException {

        String solutionFileName = "UmrechnenLoesung.txt";
        BufferedWriter solutionWriter = new BufferedWriter(new FileWriter(solutionFileName));

        ConversionTaskCreator conversionTaskCreator = new ConversionTaskCreator();
        for (int i=0; i<56; i++) {
            ConversionTask conversionTask = conversionTaskCreator.create();
            solutionWriter.write(conversionTask + " " + conversionTask.solve() + "\n");
            System.out.println(conversionTask + "              " + conversionTask.solve().getUnit().getSymbol());
        }
        solutionWriter.close();
    }
}