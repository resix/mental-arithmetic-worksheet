package mentala;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class UnitTest {

    private static final Unit UNIT_CM = new Unit(Unit.Length.CM.getFactor(), Unit.Length.CM.getSymbol());
    private static final Unit UNIT_MM = new Unit(Unit.Length.MM.getFactor(), Unit.Length.MM.getSymbol());
    private static final Unit UNIT_DM = new Unit(Unit.Length.DM.getFactor(), Unit.Length.DM.getSymbol());
    private static final Unit UNIT_M = new Unit(Unit.Length.M.getFactor(), Unit.Length.M.getSymbol());
    private static final Unit UNIT_KM = new Unit(Unit.Length.KM.getFactor(), Unit.Length.KM.getSymbol());

    @Test
    public void calculatesNeighbourUnits() {
        assertThat(UNIT_MM.getNeighbourUnit()).isEqualTo(UNIT_CM);
        assertThat(UNIT_CM.getNeighbourUnit()).isIn(UNIT_DM, UNIT_MM);
        assertThat(UNIT_DM.getNeighbourUnit()).isIn(UNIT_CM, UNIT_M);
        assertThat(UNIT_M.getNeighbourUnit()).isIn(UNIT_DM, UNIT_KM);
        assertThat(UNIT_KM.getNeighbourUnit()).isEqualTo(UNIT_M);
    }
}