package mentala;


import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isOneOf;
import static org.hamcrest.Matchers.notNullValue;

public class TaskCreatorTest {
    @Test
    public void createsRandomTask() {
        Task task = new TaskCreator().create();
        Integer firstOperand = task.getFirstOperand();
        Integer secondOperand = task.getSecondOperand();
        Task.Operator operator = task.getOperator();
        assertThat(operator, isOneOf(Task.Operator.MULTIPLY, Task.Operator.DIVIDE));
        assertThat(firstOperand, is(notNullValue()));
        assertThat(secondOperand, is(notNullValue()));
        System.out.println(task);
    }
}