package mentala;

import org.junit.Test;

import static org.assertj.core.api.Java6Assertions.assertThat;


public class TaskTest {

    @Test
    public void taskStringIsCreated() {
        assertThat(new Task(Task.Operator.ADD, 8, 7).toString()).isEqualTo("8 + 7 =");
        assertThat(new Task(Task.Operator.SUBTRACT, 8, 7).toString()).isEqualTo("8 - 7 =");
        assertThat(new Task(Task.Operator.MULTIPLY, 8, 7).toString()).isEqualTo("8 * 7 =");
        assertThat(new Task(Task.Operator.DIVIDE, 56, 7).toString()).isEqualTo("56 : 7 =");
    }

    @Test
    public void taskIsSolved() {
        assertThat(new Task(Task.Operator.ADD, 8, 7).solve()).isEqualTo(15);
        assertThat(new Task(Task.Operator.SUBTRACT, 8, 7).solve()).isEqualTo(1);
        assertThat(new Task(Task.Operator.MULTIPLY, 8, 7).solve()).isEqualTo(56);
        assertThat(new Task(Task.Operator.DIVIDE, 56, 7).solve()).isEqualTo(8);
    }

}