package mentala;


import mentala.Unit.Length;
import mentala.Unit.Weight;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ConversionTaskTest {


    private static final Unit KM = new Unit(Length.KM.getFactor(), Length.KM.getSymbol());
    private static final Unit M = new Unit(Length.M.getFactor(), Length.M.getSymbol());
    private static final Unit DM = new Unit(Length.DM.getFactor(), Length.DM.getSymbol());
    private static final Unit CM = new Unit(Length.CM.getFactor(), Length.CM.getSymbol());
    private static final Unit MM = new Unit(Length.MM.getFactor(), Length.MM.getSymbol());

    private static final Unit MG = new Unit(Weight.MG.getFactor(), Weight.MG.getSymbol());
    private static final Unit G = new Unit(Weight.G.getFactor(), Weight.G.getSymbol());
    private static final Unit KG = new Unit(Weight.KG.getFactor(), Weight.KG.getSymbol());
    private static final Unit T = new Unit(Weight.T.getFactor(), Weight.T.getSymbol());



    @Test
    public void testLengthConversions() {
        ConversionTask conversionTask = new ConversionTask(new ValueWithUnit(1.4, KM), M);
        assertThat(conversionTask.solve().getValue(), is(1400.0));

        conversionTask = new ConversionTask(new ValueWithUnit(76, M), KM);
        assertThat(conversionTask.solve().getValue(), is(0.076));

        conversionTask = new ConversionTask(new ValueWithUnit(10, CM), DM);
        assertThat(conversionTask.solve().getValue(), is(1.0));

        conversionTask = new ConversionTask(new ValueWithUnit(10, DM), CM);
        assertThat(conversionTask.solve().getValue(), is(100.0));

        conversionTask = new ConversionTask(new ValueWithUnit(200000, CM), KM);
        assertThat(conversionTask.solve().getValue(), is(2.0));

        conversionTask = new ConversionTask(new ValueWithUnit(20, KM), CM);
        assertThat(conversionTask.solve().getValue(), is(2000000.0));

        conversionTask = new ConversionTask(new ValueWithUnit(20, CM), MM);
        assertThat(conversionTask.solve().getValue(), is(200.0));
    }

    @Test
    public void testWeightConversions() {
        ConversionTask conversionTask = new ConversionTask(new ValueWithUnit(1.4, G), MG);
        assertThat(conversionTask.solve().getValue(), is(1400.0));

        conversionTask = new ConversionTask(new ValueWithUnit(0.4, T), G);
        assertThat(conversionTask.solve().getValue(), is(400000.0));
    }
}